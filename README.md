# Template attacks

*These scripts were heavily inspired by [ChipWhisperer Tutorial B8 Profiling Attacks (Manual Template Attack)](https://wiki.newae.com/Tutorial_B8_Profiling_Attacks_(Manual_Template_Attack))*

## Description

This script performs a template attack at the output of the SBox on the first round of AES.  

## Directory structure

The directory structure should be the following: 

```
├── reference
│   ├── keys.npy
│   ├── plaintexts.npy
│   └── traces.npy
├── target
│   ├── key.npy
│   ├── plaintexts.npy
│   └── traces.npy
└── template_attack.npy
```

