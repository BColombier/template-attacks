import numpy as np
from scipy.stats import multivariate_normal
import matplotlib.pyplot as plt
import numpy.ma as ma

import logging as log
import os
import time

log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)

Sbox = [0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76, 0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, 0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15, 0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, 0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, 0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, 0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8, 0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, 0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73, 0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb, 0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79, 0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08, 0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, 0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, 0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, 0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16]

HW = np.array([bin(byte).count('1') for byte in range(256)]).astype(np.uint8)

def cov(x, y):
    return np.cov(x, y)[0][1]

def compute_template(nb_bytes = 16,
                     number_of_PoIs = 5,
                     min_dist_between_PoIs = 5,
                     target_value = 'sbox_output',
                     template_type = 'HW',
                     profiling_directory = './profiling'):
    templates_directory = './templates_on_'+target_value+'_by_'+template_type
    if not os.path.exists(templates_directory):
        os.makedirs(templates_directory)
    log.info("Start template computation")
    log.info("Loading profiling data")
    ref_traces = np.load(os.path.join(profiling_directory, 'traces.npy'))
    ref_plaintexts = np.load(os.path.join(profiling_directory, 'plaintexts.npy'))
    ref_keys = np.load(os.path.join(profiling_directory, 'keys.npy'))

    nb_traces, nb_samples = np.shape(ref_traces)
    nb_plaintexts = nb_traces

    for byte in range(nb_bytes):
        log.info("Start template creation for byte {0}/{1}".format(byte, nb_bytes-1))
        log.info("Computing the Hamming weight of intermediate values")
        if target_value == 'sbox_output':
            intermediate_values = [Sbox[ref_plaintexts[plaintext_index][byte] ^ ref_keys[plaintext_index][byte]] for plaintext_index in range(nb_plaintexts)]
        elif target_value == 'key':
            intermediate_values = [ref_keys[plaintext_index][byte] for plaintext_index in range(nb_plaintexts)]
        else:
            raise ValueError("Templates computation for this intermediate value is not implemented yet... Feel free to!")
        if template_type == 'HW':
            cat_intermediate_values = HW[intermediate_values]
        elif template_type == 'value':
            cat_intermediate_values = intermediate_values
        else:
            raise ValueError("Templates computation with this type of template is not implemented yet... Feel free to!")

        log.info("Sorting traces by "+template_type+" of the intermediate value")
        counts = np.bincount(cat_intermediate_values)
        ref_traces_cat = [np.zeros((size, nb_samples)) for size in counts] # List of np arrays
        running_indexes = len(counts)*[0]
        for trace_index in range(nb_traces):
            current_trace_cat = cat_intermediate_values[trace_index]
            ref_traces_cat[current_trace_cat][running_indexes[current_trace_cat]] = ref_traces[trace_index]
            running_indexes[current_trace_cat] += 1

        log.info("Computing the average trace for each "+template_type)
        Mean_trace_per_cat = np.zeros((len(counts), nb_samples))
        for possible_cat in range(len(counts)):
            Mean_trace_per_cat[possible_cat] = np.average(ref_traces_cat[possible_cat], axis=0)

        log.info("Computing sum of absolute differences")
        sum_abs_diff = np.zeros(nb_samples)
        for i in range(len(counts)):
            for j in range(i):
                sum_abs_diff += np.abs(Mean_trace_per_cat[i] - Mean_trace_per_cat[j])

        log.info("Identifying points of interest")
        PoIs = []
        for i in range(number_of_PoIs):
            # Find the max
            PoI = sum_abs_diff.argmax()
            PoIs.append(PoI)

            # Make sure we don't pick a nearby value
            # by zeroing sum_abs_diff around the PoI
            poiMin = max(0, PoI - min_dist_between_PoIs)
            poiMax = min(PoI + min_dist_between_PoIs, len(sum_abs_diff))
            sum_abs_diff[poiMin:poiMax] = 0

        log.info("Computing the template (m, C) for each "+template_type)
        mean_vector = Mean_trace_per_cat[:,PoIs]
        covariance_matrix = np.zeros((len(counts), number_of_PoIs, number_of_PoIs))
        for possible_cat in range(len(counts)):
            for i in range(number_of_PoIs):
                for j in range(number_of_PoIs):
                    x = ref_traces_cat[possible_cat][:,PoIs[i]]
                    y = ref_traces_cat[possible_cat][:,PoIs[j]]
                    covariance_matrix[possible_cat,i,j] = cov(x, y)

        log.info("Saving template and points of interest")
        np.save(os.path.join(templates_directory, "mean_vector_byte_{0}.npy".format(byte)), mean_vector)
        np.save(os.path.join(templates_directory, "covariance_matrix_byte_{0}.npy".format(byte)), covariance_matrix)
        np.save(os.path.join(templates_directory, "points_of_interest_byte_{0}.npy".format(byte)), PoIs)

def perform_attack(nb_bytes = 16,
                   nb_considered_traces = 10,
                   target_value = 'sbox_output',
                   template_type = 'HW',
                   target_directory = './target'):
    templates_directory = './templates_on_'+target_value+'_by_'+template_type    
    log.info("Loading attack data")
    target_traces = np.load(os.path.join(target_directory, 'traces.npy'))
    target_plaintexts = np.load(os.path.join(target_directory, 'plaintexts.npy'))
    target_key = np.load(os.path.join(target_directory, 'key.npy'))

    nb_traces = np.shape(target_traces)[0]

    for byte in range(nb_bytes):
        mean_vector = np.load(os.path.join(templates_directory, "mean_vector_byte_{0}.npy".format(byte)))
        covariance_matrix = np.load(os.path.join(templates_directory, "covariance_matrix_byte_{0}.npy".format(byte)))
        PoIs = np.load(os.path.join(templates_directory, "points_of_interest_byte_{0}.npy".format(byte)))
        log.info("Performing the attack on byte {0}/{1}".format(byte, nb_bytes-1))
        P_k = np.zeros(256)
        for current_trace, current_plaintext in zip(target_traces, target_plaintexts):
            for k_hyp in range(256):
                if target_value == 'sbox_output':
                    possible_cat = Sbox[current_plaintext[byte] ^ k_hyp]
                elif target_value == 'key':
                    possible_cat = k_hyp
                else:
                    raise ValueError("Templates attack on this target value is not implemented yet... Feel free to!")
                if template_type == 'HW':
                    possible_cat = HW[possible_cat]
                elif template_type == 'value':
                    possible_cat = possible_cat
                else:
                    raise ValueError("Templates attack with this type of template is not implemented yet... Feel free to!")
                rv = multivariate_normal(mean_vector[possible_cat], covariance_matrix[possible_cat])
                P_k[k_hyp] += rv.logpdf(current_trace[PoIs])
        log.info("Most probable key byte is \"{0}\"".format(hex(P_k.argsort()[-1])[2:-1].zfill(2)))
        log.info("====> Correct key byte is \"{0}\", found at rank {1}".format(hex(target_key[byte])[2:].zfill(2),
                                                                     np.where(P_k.argsort()[::-1] == target_key[byte])[0][0]))

if __name__ == "__main__":
    target_value = 'sbox_output'
    template_type = 'value'
    compute_template(nb_bytes = 1,
                     number_of_PoIs = 5,
                     min_dist_between_PoIs = 5,
                     target_value = target_value,
                     template_type = template_type,
                     profiling_directory = './profiling')
    perform_attack(nb_bytes = 1,
                   nb_considered_traces = 20,
                   target_value = target_value,
                   template_type = template_type,
                   target_directory = './target')
